/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdfFaceAuth
 * @{
 *
 * @brief 提供人脸认证驱动的标准API接口。
 *
 * 人脸认证驱动为人脸认证服务提供统一的访问接口。获取人脸认证驱动代理后，人脸认证服务可以调用相关接口获取执行器，获取人脸认证执行器后，
 * 人脸认证服务可以调用相关接口获取执行器，获取凭据模版信息，注册人脸特征模版，进行用户人脸认证，删除人脸特征模版等。
 *
 * @since 3.2
 */

/**
 * @file IExecutor.idl
 *
 * @brief 定义执行器接口，用于获取执行器，获取凭据模版信息，注册人脸特征模版，进行用户人脸认证，删除人脸特征模版等。
 *
 * @since 3.2
 */

package ohos.hdi.face_auth.v1_0;

import ohos.hdi.face_auth.v1_0.FaceAuthTypes;
import ohos.hdi.face_auth.v1_0.IExecutorCallback;

/**
 * @brief 定义执行器接口，用于获取执行器，获取凭据模版信息，注册人脸特征模版，进行用户人脸认证，删除人脸特征模版等。
 *
 * @since 3.2
 * @version 1.0
 */
interface IExecutor {
    /**
     * @brief 获取执行器信息，人脸认证服务将执行器注册到用户认证框架时需要通过该接口获取对应信息。
     *
     * @param executorInfo 执行器信息{@link ExecutorInfo}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    GetExecutorInfo([out] struct ExecutorInfo executorInfo);
    /**
     * @brief 获取凭据模版信息。
     *
     * @param templateId 凭据模版ID。
     * @param templateInfo 凭据模版信息{@link TemplateInfo}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    GetTemplateInfo([in] unsigned long templateId, [out] struct TemplateInfo templateInfo);
    /**
     * @brief 完成执行器注册，对人脸特征模版进行对账，用于删除无效的人脸特征模板及相关信息。
     *
     * @param templateIdList 用户认证框架内由该执行器注册的人脸特征模版ID列表。
     * @param frameworkPublicKey 用户认证框架的公钥，用于校验用户认证框架私钥签名的信息。
     * @param extraInfo 其他相关信息，用于支持信息扩展。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    OnRegisterFinish([in] unsigned long[] templateIdList, [in] unsigned char[] frameworkPublicKey, [in] unsigned char[] extraInfo);
    /**
     * @brief 注册人脸特征模版。
     *
     * @param scheduleId 调度ID，用于标识一次操作请求的调度过程。
     * @param extraInfo 其他相关信息，用于支持信息扩展。
     * @param callbackObj 回调对象{@link IExecutorCallback}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    Enroll([in] unsigned long scheduleId, [in] unsigned char[] extraInfo, [in] IExecutorCallback callbackObj);
    /**
     * @brief 人脸认证。
     *
     * @param scheduleId 调度ID，用于标识一次操作请求的调度过程。
     * @param templateIdList 指定要认证的模版ID列表。
     * @param extraInfo 其他相关信息，用于支持信息扩展。
     * @param callbackObj 回调对象{@link IExecutorCallback}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    Authenticate([in] unsigned long scheduleId, [in] unsigned long[] templateIdList, [in] unsigned char[] extraInfo, [in] IExecutorCallback callbackObj);
    /**
     * @brief 人脸识别。
     *
     * @param scheduleId 调度ID，用于标识一次操作请求的调度过程。
     * @param extraInfo 其他相关信息，用于支持信息扩展。
     * @param callbackObj 回调对象{@link IExecutorCallback}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    Identify([in] unsigned long scheduleId, [in] unsigned char[] extraInfo, [in] IExecutorCallback callbackObj);
    /**
     * @brief 删除人脸特征模版。
     *
     * @param templateIdList 指定要删除的模版ID列表。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    Delete([in] unsigned long[] templateIdList);
    /**
     * @brief 取消操作请求。
     *
     * @param scheduleId 调度ID，用于标识一次操作请求的调度过程。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    Cancel([in] unsigned long scheduleId);
    /**
     * @brief 发送人脸认证功能相关操作命令。
     *
     * @param commandId 操作命令ID{@link CommandId}。
     * @param extraInfo 其他相关信息，用于支持信息扩展。
     * @param callbackObj 回调对象{@link IExecutorCallback}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    SendCommand([in] int commandId, [in] unsigned char[] extraInfo, [in] IExecutorCallback callbackObj);
}
/** @} */