/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup USB
 * @{
 *
 * @brief Defines the standard APIs of the USB function.
 *
 * This module declares the custom data types and functions used to obtain descriptors, interface objects, and request objects, and to submit requests.
 *
 * @since 3.0
 * @version 1.0
 */

/**
 * @file usbd_subscriber.h
 *
 * @brief Declares a USB driver service subscriber.
 *
 * @since 3.0
 * @version 1.0
 */

#ifndef USBD_SUBSCRIBER_H
#define USBD_SUBSCRIBER_H

#include "ipc_object_stub.h"
#include "usb_info.h"
#include "usbd_type.h"

namespace OHOS {
namespace USB {
class UsbdSubscriber : public IPCObjectStub {
public:
    explicit UsbdSubscriber() : IPCObjectStub(u"ohos.usb.IUsbManagerSubscriber") {};
    virtual ~UsbdSubscriber() = default;
        
    /**
     * @brief Subscribes to USB device events.
     *
     * @param UsbInfo USB device information.
     *
     * @return **0**: The operation is successful.
     * @return A non-0 value: The operation has failed.
     *
     * @since 3.0
     * @version 1.0
     */
    virtual int32_t DeviceEvent(const UsbInfo &info) = 0;
    
    /**
     * @brief Subscribes to port change events.
     *
     * @param portId Port ID.
     * @param powerRole Power role.
     * @param dataRole Data role.
     * @param mode Port mode.
     *
     * @return **0**: The operation is successful.
     * @return A non-0 value: The operation has failed.
     *
     * @since 3.0
     * @version 1.0
     */
    virtual int32_t PortChangedEvent(int32_t portId, int32_t powerRole, int32_t dataRole, int32_t mode) = 0;
    
    /**
     * @brief Initiates a remote request.
     *
     * @param code Command word.
     * @param data Data to be parsed.
     * @param reply Response data.
     * @param option Option data.
     *
     * @return **0**: The operation is successful.
     * @return A non-0 value: The operation has failed.
     *
     * @since 3.0
     * @version 1.0
     */
    int32_t OnRemoteRequest(uint32_t code, MessageParcel &data, MessageParcel &reply, MessageOption &option) override;

private:
    /**
     * @brief Parses USB device information.
     *
     * @param code Command word.
     * @param reply Response data.
     * @param option Option data.
     * @param info USB device information.
     *
     * @return **0**: The operation is successful.
     * @return A non-0 value: The operation has failed.
     *
     * @since 3.0
     * @version 1.0
     */
    static int32_t ParserUsbInfo(MessageParcel &data, MessageParcel &reply, MessageOption &option, UsbInfo &info);
    
    /**
     * @brief Parses USB device port information.
     *
     * @param code Command word.
     * @param reply Response data.
     * @param option Option data.
     * @param info USB device port information.
     *
     * @return **0**: The operation is successful.
     * @return A non-0 value: The operation has failed.
     *
     * @since 3.0
     * @version 1.0
     */
    static int32_t ParserPortInfo(MessageParcel &data, MessageParcel &reply, MessageOption &option, PortInfo &info);
};
} /** namespace USB */
} /** namespace OHOS */
#endif /** USBD_SUBSCRIBER_H */
